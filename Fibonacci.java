package com.fasp;
public class Fibonacci {

	private static int fib_rekursiv(int a, int b, int limit) {
		int c = a + b;
		System.out.print(c + ", ");
		if (c < limit) {
			return fib_rekursiv(b, c, limit);
		} else {
			return Integer.MAX_VALUE;
		}
	}

	private static void fib_iterativ(int a, int b, int limit) {
		int c = Integer.MIN_VALUE;
		do {
			c = a + b;
			System.out.print(c + ", ");
			a = b;
			b = c;
		} while (c < limit);
	}

	public static void main(String[] args) {
		int a = 0;
		int b = 1;
		int limit = 10000;
		System.out.println("Fibonacci, rekursiv:");
		fib_rekursiv(a, b, limit);
		System.out.println("\nFibonacci, iterativ:");
		fib_iterativ(a, b, limit);
	}
}
